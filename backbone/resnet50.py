import torch
import torch.nn as nn

from torch import Tensor
from torch.nn.modules import padding

from .bottleneck import Bottleneck, expansion

class Resnet50(nn.Module):
    in_channels: int
    num_classes: int
    cur_channels: int
    def __init__(self, in_channels = 3, num_classes = 1000) -> None:
        super(Resnet50, self).__init__()
        self.in_channels = in_channels
        self.num_classes = num_classes

        self.conv1 = nn.Conv2d(in_channels, 64, kernel_size=7, stride=2, padding=3, bias=False)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.cur_channels = 64
        
        self.block1 = self.make_block(64, 3)  # 64   -> 256
        self.block2 = self.make_block(128, 4) # 256  -> 512
        self.block3 = self.make_block(256, 6) # 512  -> 1024
        self.block4 = self.make_block(512, 3) # 1024 -> 2048

        self.avgpool = nn.AdaptiveAvgPool2d(output_size=(1, 1))
        self.fc = nn.Linear(2048, num_classes)
    
    def forward(self, input: Tensor):
        input = nn.ReLU()(self.conv1(input))
        input = self.maxpool(input)

        input = self.block1(input)
        input = self.block2(input)
        input = self.block3(input)
        input = self.block4(input)

        input = self.avgpool(input)
        input = self.fc(torch.flatten(input, 1)) # input.squeeze()

        return input

    def make_block(self, in_channels, bottleneck_num, stride=1) -> nn.Sequential:
        bottlenecks = []
        # 将当前 channels 调整到 in_channels
        bottlenecks.append(Bottleneck(self.cur_channels, in_channels, stride))
        self.cur_channels = in_channels * expansion # 经过一个Bottleneck则channels变为原来expansion倍

        for _ in range(1, bottleneck_num):
            bottlenecks.append(Bottleneck(self.cur_channels, in_channels, stride))

        return nn.Sequential(*bottlenecks)