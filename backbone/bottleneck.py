import torch
import torch.nn as nn

from torch import Tensor

expansion: int = 4

class Bottleneck(nn.Module):
    def __init__(self, in_channels: int, base_channels: int, stride=1) -> None:
        super(Bottleneck, self).__init__()
        out_channels = base_channels * expansion
        self.bottleneck = nn.Sequential(
            nn.Conv2d(in_channels, in_channels, kernel_size=1, bias=False),
            nn.BatchNorm2d(in_channels),
            nn.ReLU(),
            nn.Conv2d(in_channels, base_channels, kernel_size=3, stride=stride, padding=1, bias=False),
            nn.BatchNorm2d(base_channels),
            nn.ReLU(),
            nn.Conv2d(base_channels, out_channels, kernel_size=1, bias=False),
            nn.BatchNorm2d(out_channels)
        )
        self.relu = nn.ReLU()
        self.downsample = nn.Sequential(
            nn.Conv2d(in_channels, out_channels, kernel_size=1, stride=1,  bias=False),
            nn.BatchNorm2d(out_channels)
        )

    def forward(self, input: Tensor):
        output = self.bottleneck(input)
        raw = self.downsample(input)

        result = output + raw

        return self.relu(result)