import torch
import torch.nn as nn

from torch import Tensor
from torch.nn import Module
from typing import OrderedDict, Tuple, List, Optional, Dict

class BaseRCNN(Module):
    transform: Module
    backbone: Module
    rpn_head: Module
    rcnn_head: Module

    def __init__(self, backbone: Module, rpn_head: Module, rcnn_head: Module, transform: Module) -> None:
        super(BaseRCNN, self).__init__()
        self.backbone = backbone
        self.rpn_head = rpn_head
        self.rcnn_head = rcnn_head
        self.transform = transform

    def forward(self, inputs: List[Tensor], targets=None):
        # 记录输入原始大小
        raw_input_sizes = []
        for input in inputs:
            height, width = input.shape[-2:]
            raw_input_sizes.append((height, width))

        inputs, targets = self.transform(inputs, targets)
        features = self.backbone(inputs)

        if isinstance(features, Tensor):
            features = OrderedDict(["0", features]) # 有多重特征图情况
        
        region_proposals, rpn_loss = self.rpn_head(inputs, features, targets)
        detections, detector_loss = self.rcnn_head(features, region_proposals, inputs, targets)
        detections = self.transform.postprocess(detections, inputs, raw_input_sizes)

        loss = {}
        loss.update(detector_loss)
        loss.update(rpn_loss)

        return loss, detections


